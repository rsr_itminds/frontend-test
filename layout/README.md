# Implementing a design with HTML and CSS 
A customer bought a design for a website from a graphics company, and now he wants you to implement it.

He asks you to implement the layout in a single html file, using only plain HTML and CSS.
No JavaScript allowed, and no css frameworks or libraries.

He wants the website to use the Arial font, and double line spacing in the blog post text.
He also supplied you with three images to use on the website.

Notice how on mobile the blog post image is positioned above the text and doesn't have a shadow.

## Evaluation
You will be evaluated on your ability to implement the design using only HTML and CSS.

It does not matter if you do not manage to implement the entire design pixel-perfect, but please consider and write down how you might implement the missing pieces.
